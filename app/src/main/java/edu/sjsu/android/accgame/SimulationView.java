package edu.sjsu.android.accgame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.util.Size;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;

import static android.content.Context.SENSOR_SERVICE;

public class SimulationView extends View implements SensorEventListener {
    private Bitmap mField;
    private Bitmap mBasket;
    private Bitmap mBitmap;

    float mul = 100;


    private Display display;

   private int FIELD_WIDTH = 100;
    private int FIELD_HEIGHT = 100;


    private SensorManager sensorManager;
    private static final int BALL_SIZE = 300;
    private static final int BASKET_SIZE = 300;
    private float mXOrigin = 500;
    private float mYOrigin = 300;
    private float mHorizontalBound = 1080; //1080
    private float mVerticalBound = 1790; //like 1790

    private Particle mBall;

    public SimulationView(Context context) {
        super(context);
        // Initialize images from drawable
        Bitmap ball = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
        mBitmap = Bitmap.createScaledBitmap(ball, BALL_SIZE, BALL_SIZE, true);
        Bitmap basket = BitmapFactory.decodeResource(getResources(), R.drawable.basket);
        mBasket = Bitmap.createScaledBitmap(basket, BASKET_SIZE, BASKET_SIZE, true);
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inPreferredConfig = Bitmap.Config.RGB_565;
        mField = BitmapFactory.decodeResource(getResources(), R.drawable.field, opts);


        WindowManager mWindowManager = (WindowManager)
                context.getSystemService(Context.WINDOW_SERVICE);
        display = mWindowManager.getDefaultDisplay();

        sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_GAME);

        Point size = new Point();
        display.getSize(size);

        FIELD_WIDTH = size.x;
        FIELD_HEIGHT = size.y;

        mVerticalBound = mVerticalBound - BALL_SIZE * 2;
        mHorizontalBound = mHorizontalBound - BALL_SIZE * 2;

        Log.d("size",FIELD_WIDTH + "");
        Log.d("size",FIELD_HEIGHT + "");



        mBasket = Bitmap.createScaledBitmap(mBasket, BASKET_SIZE,BASKET_SIZE, true);
        mField = Bitmap.createScaledBitmap(mField, FIELD_WIDTH,FIELD_HEIGHT, true);
        mBitmap =  Bitmap.createScaledBitmap(mBitmap, BALL_SIZE, BALL_SIZE, true);

        mBall = new Particle();
    }


    @Override
    protected void onDraw(Canvas canvas){
       // Log.d("mdebug","drawing...");
        canvas.drawBitmap(mField,0,0,null);
        canvas.drawBitmap(mBasket,500,500,null);

        canvas.drawBitmap(mBitmap,
                (mXOrigin - BALL_SIZE / 2) + mBall.mPosX,
                (mYOrigin - BALL_SIZE / 2) - mBall.mPosY, null);
        invalidate();
    }

//    @Override
//    protected void onSizeChanged (int w, int h, int oldw, int oldh){
//        WindowManager mWindowManager = (WindowManager)
//                context.getSystemService(Context.WINDOW_SERVICE);
//        display = mWindowManager.getDefaultDisplay();
//    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            x = x * mul;
            y = y * mul;
            z = z * mul;

            Log.d("strong X: ","X: " + x);
            Log.d("strong Y: ","Y: " + y);

//            Log.d("mdebug","Time: " + event.timestamp);
//            Log.d("mdebug","X: " + mBall.mPosX);
//            Log.d("mdebug","Y: " + mBall.mPosY);
//            Log.d("mdebug","Hori width: " + FIELD_WIDTH);
//            Log.d("mdebug","Vert height: " + FIELD_HEIGHT);
//
//            Log.d("mdebug","Hori bound: " + mHorizontalBound);
//            Log.d("mdebug","Vert bound: " + mVerticalBound);
            //Log.d("mdebug","Z: " + z);
            invalidate();



            mBall.updatePosition(x, y, z, event.timestamp);
            mBall.resolveCollisionWithBounds(mHorizontalBound, mVerticalBound);
//            if (Surface.ROTATION_0) {
//                float X = event.values[0];
//                float Y = event.values[1];
//            }
//
//            else if (Surface.ROTATION_90) {
//                float X = -event.values[1];
//            float Y= event.values[0];
//            }
        }

    } //end method

    public void startSimulation(){
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }
    public void stopSimulation(){
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
