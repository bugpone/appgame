package edu.sjsu.android.accgame;

import android.util.Log;

public class Particle {
    private static final float COR = 0.7f;
    public float mPosX;
    public float mPosY;
    private float mVelX;
    private float mVelY;
    private float oldsY = 0;
    private float oldsX = 0;

    public void updatePosition(float sx, float sy, float sz, long timestamp) {
        float dt = (System.nanoTime() - timestamp) / 100000000.0f;
        mVelX += -sx * dt;
        mVelY += -sy * dt;
        mPosX += mVelX * dt;


        if(sy - oldsY > 1){
            //lift the ball up
            Log.d("strong" , "lifting...");
            mPosY += 40;
        } else {
            mPosY += mVelY * dt;
        }

//        if(sy - oldsX > 1){
//            //lift the ball up
//            Log.d("strong" , "lifting...");
//            mPosX += 20;
//        }
//
//        if(sy - oldsX < 1){
//            //lift the ball up
//            Log.d("strong" , "lifting...");
//            mPosX -= 20;
//        }

        oldsY = sy;
        oldsX = sx;

    }
    public void resolveCollisionWithBounds(float mHorizontalBound, float mVerticalBound) {
        if (mPosX > mHorizontalBound) {
            mPosX = mHorizontalBound;
            mVelX = -mVelX * COR;
        } else if (mPosX < -mHorizontalBound) {
            mPosX = -mHorizontalBound;
            mVelX = -mVelX * COR;
        }
        if (mPosY > mVerticalBound) {
            mPosY = mVerticalBound;
            mVelY = -mVelY * COR;
        } else if (mPosY < -mVerticalBound) {
            mPosY = -mVerticalBound;
            mVelY = -mVelY * COR;
        }
    }
}
